#include "syscall.h"
#include "mmalloc.h"

void assert(int condition) {
    if (!condition) {
        exit(1);
    }
}

void test_simple() {
    void *p = malloc(1);
    assert(p != NULL);
    *(char*)p = 'a';
    free(p);
}

void memset(void *ptr, char c, size_t size) {
    for (size_t i = 0; i < size; i++) {
        ((char*)ptr)[i] = c;
    }
}

void test_two() {
    intptr_t p1 = (intptr_t) malloc(1024);
    intptr_t p2 = (intptr_t) malloc(1024);
    assert(p1 < p2 || p1 >= p2 + 1024);
    assert(p2 < p1 || p2 >= p1 + 1024);
    memset((void*)p1, 'a', 1024);
    memset((void*)p2, 'a', 1024);
    free((void*)p1);
    free((void*)p2);
}

void test_large() {
    size_t size = 1<<25;
    void *p = malloc(size);
    memset(p, 'a', size);
    free(p);
}

void validate(char *p, char c, size_t size) {
    for (int j = 0; j < size; ++j) {
        assert(((char*)p)[j] == c);
    }
}

void test_realloc() {
    size_t size = 1;
    void *p = malloc(size);

    for (int i = 0; i < 25; ++i) {
        assert(p != NULL);
        char c = 'a' + i % 26;
        memset(p, c, size);
        p = realloc(p, size << 1);
        assert(p != NULL);
        validate(p, c, size);
        size <<= 1;
    }
    free(p);
}

unsigned long rdtsc() {
    unsigned int hi, lo;
    asm volatile ("rdtscp\n\tlfence" : "=a"(lo), "=d"(hi) :: "ecx");
    return (((unsigned long) hi) << 32) | lo;
}

void printl(unsigned long l) {
    char buf[17] = {[0 ... 15] = '0', [16] = '\n'};
    int i = 15;
    while (l) {
        unsigned long c = l % 16;
        buf[i] = c < 10 ? '0' + c : 'a' + c - 10;
        l >>= 4;
        i--;
    }
    write(0, buf, 17);
}

void prints(const char* s) {
    size_t size = 0;
    while (s[size]) {
        size++;
    }
    write(0, s, size);
}

void test_realloc_speed() {
    size_t size = 1 << 29;
    void *p = malloc(size);
    memset(p, 0, size);
    prints("allocated=");
    printl((unsigned long) p);

    intptr_t lp = (intptr_t) p + size;
    lp = (lp + 4095) & ~(4095UL);
    void *l = mmap((void*) (lp), 4096, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS|MAP_FIXED, -1, 0);
    assert((intptr_t) l == lp);
    memset(l, 'x', 4096);
    prints("guard page=");
    printl((unsigned long) l);

    unsigned long begin, end, result1, result2;
    begin = rdtsc();
    memset(p, 'a', size);
    end = rdtsc();
    result1 = end - begin;

    begin = rdtsc();
    p = realloc(p, size << 1);
    end = rdtsc();
    result2 = end - begin;
    prints("reallocated=");
    printl((unsigned long) p);

    prints("memset time=");
    printl(result1);
    prints("realloc time=");
    printl(result2);

    assert(result2 * 2 < result1);

    memset(p + size, 'b', size);
    validate(l, 'x', 4096);

    free(p);
    munmap(l, 4096);
}

void test_free() {
    size_t size = 1 << 20;
    void *p = malloc(size);
    memset(p, 0, size);
    free(p);

    intptr_t lp = (intptr_t) p;
    lp = lp & ~(4095UL);
    for (int i = 0; i < size; i += 4096) {
        int r = msync((void*) (lp + i), 4096, MS_ASYNC);
        assert(r == -ENOMEM);
    }
}

int main() {
    test_simple();
    test_two();
    test_large();
    test_realloc();
    test_realloc_speed();
    test_free();
    prints("OK\n");
}
